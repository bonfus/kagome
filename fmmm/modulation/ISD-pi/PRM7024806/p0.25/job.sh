#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive 
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#####SBATCH -C [scarf17|scarf18]
#SBATCH -t 152:30:00
#SBATCH -J isdp025


#module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
#module load intel/mkl/20.0.0
module load intel/fce/20.0.0
#module load intel/mpi/20.0.0

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIBRARY_PATH:$MKL_LIB

export OMP_STACKSIZE=200M
export OMP_NUM_THREADS=24
ulimit -Ss unlimited
#../../elk-6.2.8/src/elk 

#export MKL_DYNAMICS=false 
mkdir /dev/shm/ELKTMP/
cat ../elk.template geometry.in > elk.in
~/elk-9.2.12/src/elk > info.out
rm -r /dev/shm/ELKTMP/
#sleep 20h && touch STOP
#fg
