import pandas as pd
import re


e=1.602176634E-19
efg_au2si = 9.7173624292E21

isotope = input('Isotope: ')
mass = int(re.search(r'\d+', isotope).group())
symb = re.search(r'[a-zA-Z]+', isotope).group()

Vzz = float(input('Vzz: '))

info = pd.read_table('isotopedata.txt',comment='%', delim_whitespace=True, names = ["Z","A","Stable", "Symbol", "Element", "Spin", "G_factor", "Abundance", "Quadrupole"])

r = info [ (info.A==mass) & (info.Symbol == symb) ]
Q = float(r.Quadrupole) * 1e-28
S = float(r.Spin)

assert(len(r) == 1)

A=e * Vzz * efg_au2si * Q / (4 * S * (2 * S -1 ))

s = -S
for i in range(int(2*S)+1):
    omega = 3 * A * (2*abs(s)+1)/1.054571818E-34
    print(omega/6.28/1e6)
    s += 1
